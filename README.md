# Using cURL

curl is a command line application that allows you to access websites and carry out API requests from the terminal.
It does something similar to what a browser does but from the terminal.

## General usage CA Command

```bash
curl --location --request POST 'localhost:8080/api/thales' \
--header 'Content-Type: application/json' \
--data-raw '{
"header": "0119000002210000000000566730",
"max_pin_length": "12",
"zpk": "YPUK9HKYMM1SMY0N",
"source_pin_block": "01",
"dest_pin_block": "01",
"pan": "956669802929",
"command": "CA",
"tpk": "B7PC8JTS253R7OZ1X0DVDQV3IJWH1S3SF3ICI5VC46AOHPIP9",
"pin_block": "CHSMS4AS41JV206A",
"mux":"thales-mux"
}'
```

## General usage FA Command

```bash
curl --location --request POST 'localhost:8080/api/thales' \
--header 'Content-Type: application/json' \
--data-raw '{
    "header": "0119000002210000000000566730",
    "zpk": "MHS3DGQF5OUVL15TXKBNEADXIREQESJ5D",
    "zmk": "RGKU4VDVXMAKI2XZF4E9B44AUREWXAK9E",
    "command": "FA",
    "mux":"thales-mux"
}'
```


## General usage KW Command

```bash
curl --location --request POST 'localhost:8080/api/thales' \
--header 'Content-Type: application/json' \
--data-raw '{
    "header": "0119000002210000000000566730",
    "flag": "1",
    "scheme": "2",
    "mk_ac": "U87E7B360B5E6DF7101F481B145E1A809",
    "pan": "1900250202097701",
    "atc": "85",
    "trx_data_length": "45",
    "data": "00008976540000000000000003600000B000000360080804000FBBE0D5540000080101A00000000066028C0000",
    "delimiter": ";",
    "arqc": "B0B88E21B9734BB5",
    "arc": "0085",
    "command": "KW",
    "mux":"thales-mux"
}'
```



