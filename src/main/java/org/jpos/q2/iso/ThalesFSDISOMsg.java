package org.jpos.q2.iso;

import org.jpos.iso.FSDISOMsg;
import org.jpos.util.FSDMsg;

public class ThalesFSDISOMsg extends FSDISOMsg {

    public ThalesFSDISOMsg(FSDMsg msg) {
        super(msg);
    }

    @Override
    public String getMTI() {
        String command = getFSDMsg().get("command");
        if (command==null){
            return getFSDMsg().get("response");
        }
        return command;

    }

    @Override
    public boolean isRequest() {
        String command = getFSDMsg().get("response");
        if (command!=null){
            return false;
        } else {
            return true;
        }
    }
}
