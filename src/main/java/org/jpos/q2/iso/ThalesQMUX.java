package org.jpos.q2.iso;

import org.HdrHistogram.AtomicHistogram;
import org.jdom2.Element;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.*;
import org.jpos.q2.QFactory;
import org.jpos.space.*;
import org.jpos.util.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ThalesQMUX extends QMUX {
    static final String nomap = "0123456789";
    static final String DEFAULT_KEY = "41, 11";
    protected LocalSpace sp;
    protected String in, out, unhandled;
    protected String[] ready;
    protected String[] key;
    protected String ignorerc;
    protected String[] mtiMapping;
    private boolean headerIsKey;
    private boolean returnRejects;
    private LocalSpace isp; // internal space
    private Map<String,String[]> mtiKey = new HashMap<>();
    private Metrics metrics = new Metrics(new AtomicHistogram(60000, 2));

    List<ISORequestListener> listeners;
    int rx, tx, rxExpired, txExpired, rxPending, rxUnhandled, rxForwarded;
    long lastTxn = 0L;
    boolean listenerRegistered;

    public ThalesQMUX () {
        super ();
        listeners = new ArrayList<ISORequestListener>();
    }
    public void initService () throws ConfigurationException {
        Element e = getPersist ();
        sp        = grabSpace (e.getChild ("space"));
        isp       = cfg.getBoolean("reuse-space", false) ? sp : new TSpace();
        in        = e.getChildTextTrim ("in");
        out       = e.getChildTextTrim ("out");
        if (in == null || out == null) {
            throw new ConfigurationException ("Misconfigured QMUX. Please verify in/out queues");
        }
        ignorerc  = e.getChildTextTrim ("ignore-rc");
        key = toStringArray(DEFAULT_KEY, ", ", null);
        returnRejects = cfg.getBoolean("return-rejects", false);
        for (Element keyElement : e.getChildren("key")) {
            String mtiOverride = keyElement.getAttributeValue("mti");
            if (mtiOverride != null && mtiOverride.length() >= 2) {
                mtiKey.put (mtiOverride, toStringArray(keyElement.getTextTrim(), ", ", null));
            } else {
                key = toStringArray(e.getChildTextTrim("key"), ", ", DEFAULT_KEY);
            }
        }
        ready     = toStringArray(e.getChildTextTrim ("ready"));
        mtiMapping = toStringArray(e.getChildTextTrim ("mtimapping"));
        if (mtiMapping == null || mtiMapping.length != 2)
            mtiMapping = new String[] { nomap, nomap, "0022446789" };
        addListeners ();
        unhandled = e.getChildTextTrim ("unhandled");
        NameRegistrar.register ("mux."+getName (), this);
    }

    public void startService () {
        if (!listenerRegistered) {
            listenerRegistered = true;
            // Handle messages that could be in the in queue at start time
            synchronized (sp) {
                Object[] pending = SpaceUtil.inpAll(sp, in);
                sp.addListener (in, this);
                for (Object o : pending)
                    sp.out(in, o);
            }
        }
    }

    private LocalSpace grabSpace (Element e)
            throws ConfigurationException
    {
        String uri = e != null ? e.getText() : "";
        Space sp = SpaceFactory.getSpace (uri);
        if (sp instanceof LocalSpace) {
            return (LocalSpace) sp;
        }
        throw new ConfigurationException ("Invalid space " + uri);
    }

    private String[] toStringArray(String s) {
        return toStringArray(s, null,null);
    }

    private String[] toStringArray(String s, String delimiter, String def) {
        if (s == null)
            s = def;
        String[] arr = null;
        if (s != null && s.length() > 0) {
            StringTokenizer st;
            if (delimiter != null)
                st = new StringTokenizer(s, delimiter);
            else
                st = new StringTokenizer(s);

            List<String> l = new ArrayList<String>();
            while (st.hasMoreTokens()) {
                String t = st.nextToken();
                if ("header".equalsIgnoreCase(t)) {
                    headerIsKey = true;
                } else {
                    l.add (t);
                }
            }
            arr = l.toArray(new String[l.size()]);
        }
        return arr;
    }

    private void addListeners () throws ConfigurationException {
        QFactory factory = getFactory ();
        Iterator iter = getPersist().getChildren (
                "request-listener"
        ).iterator();
        while (iter.hasNext()) {
            Element l = (Element) iter.next();
            ISORequestListener listener = (ISORequestListener)
                    factory.newInstance (l.getAttributeValue ("class"));
            factory.setLogger        (listener, l);
            factory.setConfiguration (listener, l);
            addISORequestListener (listener);
        }
    }


    public String getKey (ISOMsg m) throws ISOException {
        StringBuilder sb = new StringBuilder (out);
        sb.append ('.');
        sb.append (mapMTI(m.getMTI()));
        if (headerIsKey && m.getHeader()!=null) {
            sb.append ('.');
            sb.append(ISOUtil.hexString(m.getHeader()));
            sb.append ('.');
        }
        boolean hasFields = false;
        String[] k = mtiKey.getOrDefault(m.getMTI(), key);

        for (String f : k) {
            String v = m.getString(f);

            if (v != null) {
                if ("11".equals(f)) {
                    String vt = v.trim();
                    int l = 6;
                    if (vt.length() < l)
                        v = ISOUtil.zeropad(vt, l);
                }
                if ("41".equals(f)) {
                    v = ISOUtil.zeropad(v.trim(), 16); // BIC ANSI to ISO hack
                }
                hasFields = true;
                sb.append(v);
            }
        }
        if (!hasFields)
            throw new ISOException ("Key fields not found - not sending " + sb.toString());
        return sb.toString();
    }

    private String mapMTI (String mti) {
        StringBuilder sb = new StringBuilder();

        if (mti != null) {
            for (int i=0; i<1; i++) {
                int c = mti.charAt (i) - '0';
                if (c >= 0 && c < 10)
                    sb.append (mtiMapping[i].charAt(c));
            }
        }

        return sb.toString();
    }


    /**
     * @param m message to send
     * @param timeout amount of time in millis to wait for a response
     * @return response or null
     */
    public ISOMsg request (ISOMsg m, long timeout) throws ISOException {
        String key = getKey (m);
        String req = key + ".req";

        synchronized (isp) {
            if (isp.rdp (req) != null)
                throw new ISOException ("Duplicate key '" + req + "' detected");
            isp.out (req, m);
        }
        m.setDirection(0);
        Chronometer c = new Chronometer();
        if (timeout > 0)
            sp.out (out, m, timeout);
        else
            sp.out (out, m);

        ISOMsg resp;
        try {
            synchronized (this) { tx++; rxPending++; }

            for (;;) {
                resp = (ISOMsg) isp.in (key, timeout);
                if (!shouldIgnore (resp))
                    break;
            }
            if (resp == null && isp.inp (req) == null) {
                // possible race condition, retry for a few extra seconds
                resp = (ISOMsg) isp.in (key, 10000);
            }
            synchronized (this) {
                if (resp != null)
                {
                    rx++;
                    lastTxn = System.currentTimeMillis();
                }else {
                    rxExpired++;
                    if (m.getDirection() != ISOMsg.OUTGOING)
                        txExpired++;
                }
            }
        } finally {
            synchronized (this) { rxPending--; }
        }
        long elapsed = c.elapsed();
        metrics.record("all", elapsed);
        if (resp != null)
            metrics.record("ok", elapsed);
        return resp;
    }
    public void request (ISOMsg m, long timeout, ISOResponseListener rl, Object handBack)
            throws ISOException
    {
        String key = getKey (m);

        String req = key + ".req";
        synchronized (isp) {
            if (isp.rdp (req) != null)
                throw new ISOException ("Duplicate key '" + req + "' detected.");
            m.setDirection(0);
            AsyncRequest ar = new AsyncRequest (rl, handBack);
            synchronized (ar) {
                if (timeout > 0)
                    ar.setFuture(getScheduledThreadPoolExecutor().schedule(ar, timeout, TimeUnit.MILLISECONDS));
            }
            isp.out (req, ar, timeout);
        }
        if (timeout > 0)
            sp.out (out, m, timeout);
        else
            sp.out (out, m);
        synchronized (this) { tx++; rxPending++; }
    }
    public void notify (Object k, Object value) {
        Object obj = sp.inp (k);
        if (obj instanceof ISOMsg) {
            ISOMsg m = (ISOMsg) obj;
            try {
                if (m.hasField("error")) {
                    String key = getKey (m);
                    String req = key + ".req";
                    Object r = isp.inp (req);
                    if (r != null) {
                        if (r instanceof AsyncRequest) {
                            ((AsyncRequest) r).responseReceived (m);
                        } else {
                            isp.out (key, m);
                        }
                        return;
                    }
                }
            } catch (ISOException e) {
                LogEvent evt = getLog().createLogEvent("notify");
                evt.addMessage(e);
                evt.addMessage(obj);
                Logger.log(evt);
            }
            processUnhandled (m);
        }
    }
    private boolean shouldIgnore (ISOMsg m) {
        if (m != null && ignorerc != null
                && ignorerc.length() > 0 && m.hasField(39))
        {
            return ignorerc.contains(m.getString(39));
        }
        return false;
    }

    public void stopService () {
        listenerRegistered = false;
        sp.removeListener (in, this);
    }
    public void destroyService () {
        NameRegistrar.unregister ("mux."+getName ());
    }



}
