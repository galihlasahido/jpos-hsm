package org.jpos.iso.channel;

import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.FSDISOMsg;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.util.FSDMsg;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;

import java.io.IOException;
import java.nio.charset.Charset;

public class ThalesFSDChannel extends NACChannel {
    String schema;
    Charset charset;

    @Override
    public ISOMsg createMsg() {
        FSDMsg fsdmsg = new FSDMsg (schema);
        fsdmsg.setCharset(charset);
        return new FSDISOMsg (fsdmsg);
    }

    @Override
    public void setConfiguration (Configuration cfg)
            throws ConfigurationException {
        super.setConfiguration (cfg);
        schema = cfg.get ("schema");
        charset = Charset.forName(cfg.get("charset", ISOUtil.CHARSET.displayName()));
    }

    @Override
    public void send (ISOMsg m) throws IOException, ISOException {
        if(m instanceof FSDISOMsg) {
            FSDMsg fsd = ((FSDISOMsg) m).getFSDMsg();
            fsd.setCharset(charset);
        }
        super.send(m);
    }

    @Override
    protected int getMessageLength() throws IOException, ISOException {
        int len = super.getMessageLength();
        LogEvent evt = new LogEvent (this, "fsd-channel-debug");
        evt.addMessage ("received message length: " + len);
        Logger.log (evt);
        return len;
    }
    protected void sendMessageHeader(ISOMsg m, int len) {
    }

}
