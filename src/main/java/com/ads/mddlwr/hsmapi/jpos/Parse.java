package com.ads.mddlwr.hsmapi.jpos;

import org.jdom2.JDOMException;
import org.jpos.iso.ISODate;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOUtil;
import org.jpos.util.FSDMsg;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

public class Parse {
    public static void main(String[] args) throws IOException, JDOMException, ISOException {
        String data = "00008976540000000000000003600000B000000360080804000FBBE0D5540000080101A00000000066028C0000";
        FSDMsg txm = new FSDMsg("file:cfg/hsm-");
        txm.unpack(data.getBytes());
        iterateUsingEntrySet(txm.getMap());

        txm.set("41", ISODate.getDateTime(new Date()));
        txm.set("11", ISOUtil.zeropad("210000000000566730", 18));
        txm.set("error", "00");
        txm.set("zpk", "80DF7FC22C42AE0A");
        txm.set("kcv", "C294812ED700BEC0");
        txm.set("command", "FB");
        String pak = txm.pack();
        System.out.println(pak);
    }

    public static void iterateUsingEntrySet(Map<String, String> map) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
    }

}
